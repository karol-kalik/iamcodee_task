import Form from './modules/FormValidate';
import Email from './modules/EmailValidate';
import Slider from './modules/Slider';
import Navigation from './modules/Navigation';

document.addEventListener("DOMContentLoaded", function () {
  const formValidation = document.querySelector('.l-contact--form-form');
  const requestValidation = document.querySelector('.l-page--main--form');
  const menu = document.querySelector('.l-nav--select_mobile--menu');
  const menuContext = document.querySelector('.l-nav--select ');


  document.addEventListener('scroll', Navigation);
  formValidation.addEventListener('submit', e => Form(e))
  requestValidation.addEventListener('submit', e => Email(e))
  menu.addEventListener('click', () => menuContext.classList.toggle('menu--active'));


  Slider();
})
