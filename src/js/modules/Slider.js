const Slider = () => {

  const img = document.querySelector('.l-slider--image');
  const textSlider = document.querySelector('.l-slider--textSlider p');
  const dots = [...document.querySelectorAll('.l-slider--dots span')];
  let active = 0;

  const sliders = ["../../content/content_2_bg.png", "../../content/content_2_bg.png", "../../content/content_2_bg.png"];
  const textSliderArr = [
    "“Arkenea thinks outside the box and offer solutions through the creative process of developing apps.They care about their customers.” <br />Hypnotherapist and Best Selling Author <br />-Glenn Harrold ",

    "“Text slider two.” <br />Hypnotherapist and Best Selling Author <br />-Glenn Harrold  ",

    "“Text slider three.” <br />Hypnotherapist and Best Selling Author <br />-Glenn Harrold  "
  ]

  const changeDot = function () {
    const activeDot = dots.findIndex(dot => dot.classList.contains('active'));
    dots[activeDot].classList.remove('active');
    dots[active].classList.add('active');

  }

  const slider = function () {
    active++;
    if (active === sliders.length) {
      active = 0;
    }
    img.src = sliders[active];
    textSlider.innerHTML = textSliderArr[active];

    changeDot();
  }




  const keyChangeSlide = function (e) {
    if (e.keyCode == 37 || e.keyCode == 39) {
      clearInterval(indexInterval);
      e.keyCode == 37 ? active-- : active++;
      if (active === sliders.length) active = 0;
      else if (active < 0) active = sliders.length - 1;

      img.src = sliders[active];
      textSlider.innerHTML = textSliderArr[active];
      changeDot();
      indexInterval = setInterval(slider, 3000)
    }

  }


  const dotClick = function () {
    clearInterval(indexInterval);
    if (this.id === "one") {
      active = 0;
      changeDot();
      indexInterval = setInterval(slider, 3000);
      img.src = sliders[active];
      textSlider.innerHTML = textSliderArr[active];
    } else if (this.id === "two") {
      active = 1;
      changeDot();
      indexInterval = setInterval(slider, 3000)
      img.src = sliders[active];
      textSlider.innerHTML = textSliderArr[active];
    } else if (this.id === "three") {
      active = 2;
      changeDot();
      indexInterval = setInterval(slider, 3000)
      img.src = sliders[active];
      textSlider.innerHTML = textSliderArr[active];
    }



  }
  let indexInterval = setInterval(slider, 5000)
  window.addEventListener('keydown', keyChangeSlide)
  dots.forEach(dot => dot.addEventListener('click', dotClick))

}

export default Slider;
