const Navigation = (e) => {
  const navBar = document.querySelector('.l-nav')
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    navBar.classList.add('active');
  } else {
    navBar.classList.remove('active');
  }

};

export default Navigation;
