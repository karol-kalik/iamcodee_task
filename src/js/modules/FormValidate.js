const Form = (e) => {
  const name = document.querySelector(".form-name");
  const email = document.querySelector(".form-email");
  const message = document.querySelector(".form-message");

  if (name.value === "") {
    e.preventDefault();
    alert("Enter your name");
  } else if (email.value === "") {
    e.preventDefault();
    alert("Enter your email address");
  } else if (message.value === "") {
    alert("Write something to us");
    e.preventDefault();
  }
};

export default Form;
